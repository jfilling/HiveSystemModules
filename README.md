This repository contains module maps for system modules used by the Swift Package Manager for building HiveController.

To use this package, dependent modules will need to do two things:

	1) Declare the package dependency in the dependent package declaration, e.g.:

		let package = Package(
			name: "SomePackageThatDependsOnSystemModules",
			dependencies: [
				.Package(url: "https://bitbucket.org/PassiveLogic/hivesystemmodules", majorVersion: 1)
			]

	2) Import the specific module (not the package) in the Swift source file that needs the symbols, e.g.:

		import CPostgres