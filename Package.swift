// swift-tools-version:4.0
//==================================================================================================
//  Package.swift
//
//  Description: Provides package declaration for the system modules required by HiveController.
//
//  Created by Phil on 12/14/16.
//  Copyright (c) 2016 PassiveLogic. All rights reserved.
//==================================================================================================

import PackageDescription

let package = Package(
    name: "HiveSystemModules"
)
